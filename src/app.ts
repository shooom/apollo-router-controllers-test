import { graphiqlExpress, graphqlExpress } from 'apollo-server-express';
import * as bodyParser from 'body-parser';
import 'reflect-metadata';
import { createExpressServer } from 'routing-controllers';
import { UserController } from './modules/user/application/controllers/UserController';

import schema from './modules/user/application/graphql/data/schema';

const APP_PORT = 3000;

const app = createExpressServer({
    controllers: [
        UserController,
    ],
});

app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));

app.use('/graphiql', bodyParser.json(), graphiqlExpress({endpointURL: '/graphql'}));

app.listen(APP_PORT, () => {
    // tslint:disable-next-line:no-console
    console.log(`GraphiQL is now running on http://localhost:${APP_PORT}/graphiql`);
});
