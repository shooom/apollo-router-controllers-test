import {Body, Get, JsonController, Param, Post } from 'routing-controllers';
import { User } from '../../domain/interfaces/IUser';
import { UserRepository } from '../../infrastructure/UserRepository';

@JsonController()
export class UserController {
    public userRepository: UserRepository = new UserRepository();

    @Get('/users')
    public getAll(): User[] {
       return this.userRepository.findAll();
    }

    @Get('/users/:id')
    public getOne(@Param('id') id: number): User {
       return this.userRepository.findById(id);
    }

    @Post('/users')
    public post(@Body() user: User): void {
       this.userRepository.insert(user);
    }
}
