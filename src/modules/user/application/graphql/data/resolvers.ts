import { UserInput } from '../../../domain/interfaces/IUser';
import { UserRepository } from '../../../infrastructure/UserRepository';

const userRepository = new UserRepository();

interface UserArgs {
    userId: number;
}

const resolvers = {
    Query: {
        allUsers: () => {
        return userRepository.findAll();
        },
        user: (_: any, args: UserArgs) => {
            return userRepository.findById(args.userId);
        },
    },
    Mutation: {
        addUser: (input: UserInput) => {
            // tslint:disable-next-line:no-console
            console.log(input);
            userRepository.insert(input);
            return input;
        },
    },
};

export default resolvers;
