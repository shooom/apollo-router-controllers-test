import { makeExecutableSchema } from 'graphql-tools';
import resolvers from './resolvers';

const typeDefs = `
    input UserInput {
        name: String
        family: String
    }
    type TVChannel {
        id: Int
        name: String
        owner: String
        series: [Series]
    }

    type Series {
        id: Int
        name: String
        channel: TVChannel
        users: [User]
    }

    type User {
        id: Int
        name: String
        family: String
        series: Series
    }

    type Query {
        allUsers: [User]
        user(userId: Int): User
    }

    type Mutation {
        addUser(input: UserInput): User
    }
`;

const schema = makeExecutableSchema(
    {
        typeDefs,
        resolvers: resolvers as any,
    });

export default schema;
