import { TVChannel } from '../interfaces/ITVChannel';

export const channels: TVChannel[] = [
    {
        name: 'HBO',
        owner: 'Home Box Office, Inc. (Time Warner)',
    },
    {
        name: 'FX',
        owner: '21st Century Fox (FX Networks, LLC)',
    },
];
