import { User } from '../interfaces/IUser';

export const users: User[] = [
    {
        name: 'John Snow',
        family: 'Stark',
    },
    {
        name: 'Ramsy Bolton',
        family: 'Bolton',
    },
    {
        name: 'Tyrion Lanister',
        family: 'Lanister',
    },
];