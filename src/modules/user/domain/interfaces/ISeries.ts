import { TVChannel } from '../interfaces/ITVChannel';

export interface Series {
    name: string;
    channel: TVChannel;
}
