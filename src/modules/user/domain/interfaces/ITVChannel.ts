export interface TVChannel {
    name: string;
    owner: string;
}
