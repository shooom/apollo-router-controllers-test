import { Series } from '../interfaces/ISeries';

interface UserInput {
    name: string;
    family?: string;
}

interface User {
    name: string;
    family?: string;
    series?: Series;
}

export { User, UserInput };
