import { channels as data } from '../domain/data/TVChannelsData';
import { TVChannel } from '../domain/interfaces/ITVChannel';

export class TVChannelRepository {
    public findById(id: number): TVChannel {
        return data[id];
    }

    public findALl(): TVChannel[] {
        return data;
    }
}
