import { users as data } from '../domain/data/UsersData';
import { User } from '../domain/interfaces/IUser';

class UserRepository {
    public findAll(): User[] {
        return data;
    }

    public findById(id: number): User {
        const result = data[id];
        // tslint:disable-next-line:no-console
        console.log(` user id = ${id}`);
        // tslint:disable-next-line:no-console
        console.log(result);
        return result;
    }

    public insert(user: User): void {
        data.push(user);
    }
}

export { User, UserRepository };
